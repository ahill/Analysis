import os
import ROOT
import array
from UserCode.DataAnalysis.AnalysisBase import AnalysisBase
from UserCode.DataAnalysis.UniformRejectionSampling import *

class Analysis(AnalysisBase):

    def beginJob(self,fOut):
        """ starts this analysis class """

        setattr(self,'recoilEstimators',['tkmet','puppimet','npvmet','ntnpv'])
        setattr(self,'dataEras',['','C','D','E','F','G','H'])
        
        #uniform rejection sampling
        setattr(self,'ursampler',UniformRejectionSampling('{0}/src/UserCode/DataAnalysis/data/wmass/trainweights.pck'.format(os.environ['CMSSW_BASE'])))
        self.ursampler.defineAcceptanceRanges(5,95)

        #init ntuple
        varNames=['isGood','wgt','rho','nvert','isZ','isW']
        for m in self.recoilEstimators:
            for v in ['pt','logpt','phi','ht','loght','sphericity','dphi2leadneut','dphi2leadch']:
                varNames.append(m+'_'+v)
        varNames += [ 'tkmet_n','tkmet_m','logtkmet_m','cosdphi_ntnpv_tk', 'absdphi_ntnpv_tk', 'dphi_puppi_tk' ]
        for v in ['vx','vy','vz','mindz']:
            varNames.append(v)
        for lead in ['leadneut','leadch']:
            for v in ['pt','phi']:
                varNames.append('%s_%s'%(lead,v))
        for v in ['pt','logpt','phi','eta']:
            varNames.append( 'vis_'+v )
        varNames += ['nJets']
            
        varNames += ['lne1','e2','trueh','truephi']
        varNames += ['w_lne1','w_e2','w_trueh','w_sphericity']

        fOut.cd()
        self.tuple = ROOT.TNtuple("data","data",":".join(varNames))

        #book histograms
        self.histos={}
        for era in self.dataEras:
            for cat in ['z','w']:
                cat='{0}_2016{1}'.format(cat,era)
                self.histos[(cat,'nvert')]    = ROOT.TH1F('{0}_{1}'.format(cat,'nvert'),';Vertex multiplicity;Events',50,0,50)
                self.histos[(cat,'mindz')]    = ROOT.TH1F('{0}_{1}'.format(cat,'mindz'),';Distance to closest vtx [cm];Events',50,0,2)
                self.histos[(cat,'vispt')]    = ROOT.TH1F('{0}_{1}'.format(cat,'vispt'),';Transverse momentum [GeV];Events',50,0,100)
                self.histos[(cat,'viseta')]   = ROOT.TH1F('{0}_{1}'.format(cat,'viseta'),';Pseudo-rapidity;Events',50,-3,3)
                self.histos[(cat,'vism')]     = ROOT.TH1F('{0}_{1}'.format(cat,'vism'),';Mass;Events',50,91.-18.,91.+18.)
                
                for m in self.recoilEstimators:
                    self.histos[(cat,m,'pt')]         = ROOT.TH1F('{0}_{1}_{2}'.format(cat,m,'pt'),';|h_{T}| [GeV];Events',50,0,100)
                    self.histos[(cat,m,'phi')]        = ROOT.TH1F('{0}_{1}_{2}'.format(cat,m,'phi'),';#phi(h_{T}) [rad];Events',50,-3.15,3.15)
                    self.histos[(cat,m,'px')]         = ROOT.TH1F('{0}_{1}_{2}'.format(cat,m,'px'),';h_{x} [GeV];Events',50,-100,100)
                    self.histos[(cat,m,'py')]         = ROOT.TH1F('{0}_{1}_{2}'.format(cat,m,'py'),';h_{y} [GeV];Events',50,-100,100)
                    self.histos[(cat,m,'sphericity')] = ROOT.TH1F('{0}_{1}_{2}'.format(cat,m,'sphericity'),';Sphericity;Events',50,0,1)
                    self.histos[(cat,m,'ht')]         = ROOT.TH1F('{0}_{1}_{2}'.format(cat,m,'ht'),';#Sigma |p_{T}| [GeV];Events',100,0,1000)
                    self.histos[(cat,m,'upar')]       = ROOT.TH1F('{0}_{1}_{2}'.format(cat,m,'upar'),';u_{//} [GeV];Events',50,-150,100)
                    self.histos[(cat,m,'uperp')]      = ROOT.TH1F('{0}_{1}_{2}'.format(cat,m,'uperp'),';u_{T} [GeV];Events',50,-100,100)
                    for v in ['pt','sphericity','upar','uperp']:
                        for p,nbins,xmin,xmax in [('rho',20,0,40),
                                                  ('nvert',20,0,60),
                                                  ('ht',100,0,500),
                                                  ('pt',20,0,50),
                                                  ('mindz',20,0,1),
                                                  ('vz',20,0,10)]:
                            h=self.histos[(cat,m,v)]
                            self.histos[(cat,m,v,p)]=ROOT.TH2F('%s_vs_%s'%(h.GetName(),p),
                                                               ';%s;%s;Events'%(h.GetXaxis().GetTitle(),p),
                                                               h.GetNbinsX(),h.GetXaxis().GetXmin(),h.GetXaxis().GetXmax(),
                                                               nbins,xmin,xmax)

        for key in self.histos:
            self.histos[key].Sumw2()
            self.histos[key].SetDirectory(fOut)

        return

    def getWl(self,tree,pdgId=13):
        """ standard W selection """

        #trigger
        if tree.HLT_BIT_HLT_IsoMu24_v==0 or tree.HLT_BIT_HLT_IsoTkMu24_v==0 : return None

        #lepton
        if tree.nLepGood!=1                  : return None
        if abs(tree.LepGood_pdgId[0])!=pdgId : return None
        if tree.LepGood_pt[0]<25             : return None
        if abs(tree.LepGood_eta[0])>2.4      : return None
        if tree.LepGood_tightId[0]==0        : return None
        if tree.LepGood_relIso03[0]>0.05     : return None

        lp4=ROOT.TLorentzVector(0,0,0,0)
        lp4.SetPtEtaPhiM(tree.LepGood_pt[0],tree.LepGood_eta[0],tree.LepGood_phi[0],tree.LepGood_mass[0])

        return lp4

    def getZll(self,tree,pdgId=13):
        """standard Z selection"""

        #trigger
        if tree.HLT_BIT_HLT_IsoMu24_v==0 or tree.HLT_BIT_HLT_IsoTkMu24_v==0 : return None

        #lepton
        lp4=[]
        if tree.nLepGood!=2 : return None
        for i in xrange(0,2):
            if abs(tree.LepGood_pdgId[i])!=pdgId : return None
            if tree.LepGood_tightId[i]==0        : return None
            if tree.LepGood_relIso03[i]>0.05     : return None
            if tree.LepGood_pt[i]<25             : return None
            if abs(tree.LepGood_eta[i])>2.4      : return None
            lp4.append( ROOT.TLorentzVector(0,0,0,0) )
            lp4[-1].SetPtEtaPhiM(tree.LepGood_pt[i],tree.LepGood_eta[i],tree.LepGood_phi[i],tree.LepGood_mass[i])
        
        #Z mass window
        z=lp4[0]+lp4[1]
        if abs( z.M()-91.)>15. : return None

        return z

    def getEventWeight(self,tree):
        """determine this event weight"""

        if tree.isData : 
            era=self.tag.split('2016')[1]
            dataEraWgts={('','nom'):1.0,('2016'+era,'nom'):1.0}
            return 1.0,dataEraWgts
        
        #generated pileup
        puWgts=self.puWgt.getWeights(int(tree.nTrueInt))

        #generator level weights
        wgt=tree.genWeight
        normHistos=self.getNormHistos()
        if normHistos and 'SumGenWeights' in normHistos:
            wgt /= normHistos['SumGenWeights'].GetBinContent(1)
        return wgt,puWgts
        
    def run(self,tree):
        """process single event"""

        #select
        wl=self.getWl(tree)
        zll=self.getZll(tree)
        if not wl and not zll : return False        
        cat,visV='w',wl
        if not visV: cat,visV='z',zll

        #event weights
        wgt,puWgts=self.getEventWeight(tree)

        #init values to store in tuple
        tupleVals=[1.0,
                   wgt*puWgts[('','nom')],
                   tree.rho,
                   tree.nVert,
                   1 if cat=='z' else 0,
                   1 if cat=='w' else 0]

        #loop over recoil estimators
        for m in self.recoilEstimators:
            recoil_pt         = getattr(tree,'{0}_recoil_pt'.format(m))
            recoil_phi        = getattr(tree,'{0}_recoil_phi'.format(m))
            recoil_sphericity = getattr(tree,'{0}_recoil_sphericity'.format(m))
            recoil_ht         = recoil_pt/recoil_sphericity if recoil_sphericity>0 else 0.
            h=ROOT.TVector2(recoil_pt*ROOT.TMath.Cos(recoil_phi),recoil_pt*ROOT.TMath.Sin(recoil_phi))
            
            ev=ROOT.TVector2(visV.X(),visV.Y())
            ev=ev.Unit()                
            upar=h*ev
            uperp=h-h.Proj(ev)
            uperp=uperp.Mod() if ROOT.TVector2.Phi_mpi_pi(uperp.Phi()-ev.Phi())>0 else -uperp.Mod()

            dphi2leadneut = getattr(tree,'{0}_dphi2leadneut'.format(m))
            dphi2leadch   = getattr(tree,'{0}_dphi2leadch'.format(m))

            tupleVals += [recoil_pt,ROOT.TMath.Log(recoil_pt),recoil_phi,recoil_ht,ROOT.TMath.Log(recoil_ht),recoil_sphericity,dphi2leadneut,dphi2leadch]

            #fill control histograms
            for eraKey in puWgts:

                era,puVar=eraKey
                if puVar!='nom' : continue
                if era=='' : era='2016'
                pwgt=wgt*puWgts[eraKey]
                catera='{0}_{1}'.format(cat,era)

                self.histos[(catera,m,'pt')].Fill(recoil_pt,pwgt)
                self.histos[(catera,m,'phi')].Fill(recoil_phi,pwgt)
                self.histos[(catera,m,'px')].Fill(h.Px(),pwgt)
                self.histos[(catera,m,'py')].Fill(h.Py(),pwgt)
                self.histos[(catera,m,'sphericity')].Fill(recoil_sphericity,pwgt)
                self.histos[(catera,m,'ht')].Fill(recoil_ht,pwgt)
                self.histos[(catera,m,'upar')].Fill(upar,pwgt)
                self.histos[(catera,m,'uperp')].Fill(uperp,pwgt)
                for p,pval in [('rho',tree.rho),
                               ('nvert',tree.nVert),
                               ('ht',recoil_ht),
                               ('pt',visV.Pt()),
                               ('mindz',abs(tree.mindz)),
                               ('vz',abs(tree.vz))
                               ]:
                    self.histos[(catera,m,'pt',p)].Fill(recoil_pt,pval,pwgt);
                    self.histos[(catera,m,'sphericity',p)].Fill(recoil_sphericity,pval,pwgt)
                    self.histos[(catera,m,'upar',p)].Fill(upar,pval,pwgt)
                    self.histos[(catera,m,'uperp',p)].Fill(uperp,pval,pwgt)
                    
                #do this only once
                if m!='tkmet': continue
                self.histos[(catera,'nvert')].Fill(tree.nVert,pwgt)
                self.histos[(catera,'mindz')].Fill(tree.mindz,pwgt)
                self.histos[(catera,'vispt')].Fill(visV.Pt(),pwgt)
                self.histos[(catera,'viseta')].Fill(visV.Eta(),pwgt)
                self.histos[(catera,'vism')].Fill(visV.M(),pwgt)


        

        htk           = tree.tkmet_recoil_pt       
        phitk         = tree.tkmet_recoil_phi
        phintnpv      = tree.ntnpv_recoil_phi
        phipuppi      = tree.puppimet_recoil_phi
        dphi_ntnpv_tk = ROOT.TVector2.Phi_mpi_pi(phintnpv-phitk)
        dphi_puppi_tk = ROOT.TVector2.Phi_mpi_pi(phipuppi-phitk)
        sgnNTNPV=-1.0 if dphi_ntnpv_tk<0 else 1.0

        tupleVals += [ tree.tkmet_n, tree.tkmet_m, ROOT.TMath.Log(tree.tkmet_m), ROOT.TMath.Cos(dphi_ntnpv_tk), abs(dphi_ntnpv_tk), dphi_puppi_tk*sgnNTNPV ]

        tupleVals +=  [ tree.vx,  tree.vy, tree.vz, 
                        tree.mindz, 
                        tree.leadneut_pt, tree.leadneut_phi, 
                        tree.leadch_pt,   tree.leadch_phi,
                        visV.Pt(), ROOT.TMath.Log(visV.Pt()), visV.Eta(), visV.Phi() ]

        #jet multiplicity
        tupleVals += [ tree.nJet_Clean ]

        #targets to regress
        hmc   = tree.truth_recoil_pt
        phimc = tree.truth_recoil_phi
        lne1  = -5 if hmc==0 or htk==0 else ROOT.TMath.Log(hmc/htk)
        e2    = ROOT.TVector2.Phi_mpi_pi(phimc-phitk) if hmc>0 else -4
        tupleVals += [lne1,e2,hmc,phimc]
        
        #weights for uniform sampling
        weightVars = [(lne1,'lne1'),(e2,'e2'),(hmc,'trueh'),(tree.tkmet_recoil_sphericity,'tkmet_sphericity')]
        tupleVals += [ 1.0 if self.ursampler.accept(v[0],v[1]) else 0.0 for v in weightVars]

        self.tuple.Fill(array.array("f",tupleVals))

        return True

    def finalize(self,output):
        """add result to output"""
        
        output.cd()
        self.tuple.Write()

        output=output.mkdir('recoilAnalysis')
        output.cd()
        for key in self.histos:
            self.histos[key].SetDirectory(output)
            self.histos[key].Write()
