import pickle
import ROOT
import numpy as np

class UniformRejectionSampling:
    """A simple implementation of the acceptance-rejection method to convert 
       a MC distribution to a uniform distribution"""
    def __init__(self,url):
        with open(url,'r') as cache:
            self.pdfcdfMap=pickle.load(cache)
        self.accRanges={}
        self.n_min={}
    def defineAcceptanceRanges(self,qmin=5,qmax=95):
        for key in self.pdfcdfMap:
            pdf,_,yq=self.pdfcdfMap[key]
            rBins = [ pdf.GetXaxis().FindBin( yq[x] ) for x in [qmin,qmax] ]
            self.accRanges[key] = [ pdf.GetXaxis().GetBinLowEdge(rBins[0]),
                                    pdf.GetXaxis().GetBinUpEdge(rBins[1]) ]
            self.n_min[key]     = min([ pdf.GetBinContent(x) for x in rBins ])
    def accept(self,x,key):
        rDef=self.accRanges[key]
        pdf,_,_=self.pdfcdfMap[key]
        if x<rDef[0] or x>rDef[1]:
            return True
        u=np.random.uniform()
        ibin=pdf.GetXaxis().FindBin(x)
        nx=pdf.GetBinContent(ibin)
        return True if u<self.n_min[key]/nx else False
