import ROOT
import pickle
import optparse
import shlex
from UserCode.DataAnalysis.ProgressBar    import DrawProgressBar
from UserCode.DataAnalysis.PileupWeighter import PileupWeighter

class AnalysisBase():

    def __init__(self,argstr):
        """ starts this analysis class """

        self.normHistos=None
        self.puWgt=None        

        #configure
        try:
            parser = optparse.OptionParser()
            parser.add_option('--cache',  dest='cache', default=None ,type='string')
            parser.add_option('--pu',     dest='pu',    default=None ,type='string')
            parser.add_option('--tag',    dest='tag',   default=None, type='string')
            parser.add_option('--maxEvents',   dest='maxEvents',   default=-1, type=int)
            parser.add_option('--startEvent',   dest='startEvent',   default=0, type=int)
            (opt,args) = parser.parse_args(shlex.split(argstr))            

            self.maxEvents=opt.maxEvents
            self.startEvent=opt.startEvent
            self.tag=opt.tag

            if opt.cache and opt.tag:
                try:
                    with open(opt.cache,'r') as cache:
                        self.normHistos=pickle.load(cache)[opt.tag]                    
                    print 'Normalization cache retrieved for',opt.tag
                except Exception,e:
                    print 'Unable to read normalization histos for',opt.tag,'from',opt.cache
                    print e

            if opt.pu and self.normHistos:
                try:
                    self.puWgt=PileupWeighter(self.normHistos['nTrueInt'],opt.pu)
                    print 'Pileup weighter started for',opt.tag
                except Exception,e:
                    print 'Unable to start PileupWeighter for',opt.tag
                    print e

        except Exception as e:
            pass

        return

    def getNormHistos(self):
        return self.normHistos
        
    def _beginJob(self,fOut):
        """start of the job"""
        return self.beginJob(fOut)

    def _run(self,tree):
        """process events"""

        nentries=tree.GetEntries()
        if self.maxEvents>0 : 
            nentries=self.startEvent+self.maxEvents+1

        for i in xrange(self.startEvent,nentries):
            if i%5000==0: DrawProgressBar(percent=float(i)/float(nentries))
            tree.GetEntry(i)
            self.run(tree)            

        return True

    def _finalize(self,output):
        """add result to output"""        
        return self.finalize(output)
            
