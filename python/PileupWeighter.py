import ROOT
import sys

class PileupWeighter:
    """
    A class to handle the weighting of pileup in simulation.
    Requires a file with target distributions (assuming generated with scripts/runPileupEstimation.py)
    and an histogram with the originally generated pileup distribution
    """

    def __init__(self,genH,targetFile):

        #make sure histogram is normalized
        genH=genH.Clone('normgen')
        genH.Scale(1./genH.Integral())

        #compute weights to scale to data
        fIn=ROOT.TFile.Open(targetFile)
        tags = ['']
        tags += [ key.GetName() for key in fIn.GetListOfKeys() if key.IsFolder() ]
        self.wgtsH=self.computePileupWeights(fIn,tags,genH)
        fIn.Close()

    def computePileupWeights(self,fIn,tags,genH):
        """parse the pileup histos created with runPileupEstimation.py"""
        wgtsH={}
        for t in tags:
            for var in ['nom','up','down']:
                hname='pu_'+var
                if len(t)!=0 : hname=t+'/'+hname
                wgtsH[(t,var)]=fIn.Get(hname).Clone('puwgts_%s_%s'%(t,var))
                wgtsH[(t,var)].Scale(1./wgtsH[(t,var)].Integral())
                for xbin in xrange(1,wgtsH[(t,var)].GetNbinsX()+1):
                    genval=genH.GetBinContent(xbin)
                    obsval=wgtsH[(t,var)].GetBinContent(xbin)
                    wgtsH[(t,var)].SetBinContent(xbin,obsval/genval if genval>0 else 0.)
                    wgtsH[(t,var)].SetBinError(xbin,0.)
                wgtsH[(t,var)].SetDirectory(0)
        return wgtsH

    def getWeights(self,genPu):
        """returns all possible pileup weights (assumes histograms start at 0 pileup)"""
        wgts={}
        for key in self.wgtsH:
            wgts[key]=self.wgtsH[key].GetBinContent(genPu+1)
        return wgts


def main():
    """testing functionality"""

    pu,cache,tag=sys.argv[1:4]

    import pickle
    with open(cache,'r') as cache: 
        normHistos=pickle.load(cache)[tag]['pileup']

    puWgtr=PileupWeighter(normHistos,pu)
    print 'Weights for pu=20'
    print puWgtr.getWeights(20)

if __name__ == "__main__":
    sys.exit(main())
