# Recoil ntuplizer

## Installation

Check if any update is needed from https://github.com/WMass/cmgtools-lite. Otherwise do the following

```
cmsrel CMSSW_8_0_25
cd CMSSW_8_0_25/src
cmsenv
git cms-init
cp /afs/cern.ch/user/c/cmgtools/public/sparse-checkout_80X_heppy .git/info/sparse-checkout
git remote add cmg-central https://github.com/CERN-PH-CMG/cmg-cmssw.git -f  -t heppy_80X
git clone -o cmg-central https://github.com/WMass/cmgtools-lite.git -b 80X CMGTools
git clone ssh://git@gitlab.cern.ch:7999/psilva/Analysis.git  UserCode/DataAnalysis
scram b -j 8
```

## Create base ntuples on lxplus

Start by creating a proxy
```
export LSB_JOB_REPORT_MAIL=N
voms-proxy-init --voms cms --out myprox_509 —valid 172:00
export X509_USER_PROXY=`pwd`/myprox_509
```

Launch the cfg file to run on data and MC
```
heppy_batch.py -o  /eos/cms/store/temp/user/psilva/MC    cfg/wmass/run_recoil_cfg.py -b 'bsub -q 2nw < ./batchScript.sh'
heppy_batch.py -o  /eos/cms/store/temp/user/psilva/Data  cfg/wmass/run_recoil_cfg.py -b 'bsub -q 2nw < ./batchScript.sh' --option runData=True
```

Check the integrity
```
cmgListChunksToResub .
heppy_check.py .
```

Add the results (max size given in Gb)
```
haddChunks.py --max-size=1 -c ./
```

## Create friends tree with scale factors and recoil variables

Test a processing module (update the module list accordingly in postproc_batch.py)
```
python $CMSSW_BASE/src/CMGTools/WMass/python/postprocessing/postproc.py \
       --moduleList RECOILTEST_MODULES --max-entries 100 --friend \
       /tmp/psilva/ \
       /eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest/DYJetsToLL_M50_part1/treeProducerWMass/tree.root
```
Launch the creation of friends tree
```
python $CMSSW_BASE/src/CMGTools/WMass/python/postprocessing/postproc_batch.py \
       --moduleList RECOILTEST_MODULES --friend --signals DYJetsToLL,WJetsToLNu -q 1nd \
       --run ../../CMGTools/WMass/python/postprocessing/lxbatch_runner.sh \
       /eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest/ \
       /eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest_post
```
In case some fails you can resubmit with the options `-c chunk_number` and `-d dataset_name`.
Merge the chunks preserving the order by which they were run, so that trees can be made friends
```
python scripts/mergeOutputs.py --sep .chunk /eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest_post/
```

## Start the local analysis (the following is to be moved to a steer script once happy)

Store cache files with the pileup distributions and normalization weights
```
python scripts/produceNormalizationCache.py -o data/wmass/GenInfo.pck --histoList "treeProducerWMass/tree.root:SumGenWeights+tree.nTrueInt(100,0,100)" -i /eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest
python scripts/runPileupEstimation.py
```
Run over a signal file and create sampling weights for training (may take a while)
```
args="--cache $CMSSW_BASE/src/UserCode/DataAnalysis/data/wmass/GenInfo.pck --pu $CMSSW_BASE/src/UserCode/DataAnalysis/data/wmass/pileup.root"
python scripts/analysisWrapper.py --only WJetsToLNu_part1 -o /tmp/ -q local --args "${args}"
python test/wmass/prepareTrainingWeights.py /tmp/WJetsToLNu_part1.root
```
Now run the full analysis over data and MC
(can run locally using -j njobs instead of -q condor_queue)
```
python scripts/analysisWrapper.py -q workday -o /eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest_regress-v2/ --args "${args}"
```
Merge the outputs and make combined plots
```
python scripts/mergeOutputs.py --sep _part --hadd "-T" /eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest_regress/ 
python scripts/plotter.py -j data/wmass/samples.json -i /eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest_regress/ -O results/plots -l 1 --fdir recoilAnalysis -l 35922 --mcUnc 0.025 --scaleToData --doDataOverMC
python scripts/plotter.py -j data/wmass/samples.json -i /eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest_regress/ -O results/plots -l 1 --fdir recoilAnalysis -l 35922 --mcUnc 0.025 --only z_2016_ --doDataOverMC -o plotter_z
```