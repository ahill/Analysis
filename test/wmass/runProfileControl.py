#!/usr/bin/env python

import os
import sys
import ROOT
from array import array

def getProfiles(h,step=1):
    """build the median and 68%CI profiles"""

    try:
        if h.GetEntries()==0 : return None,None
    except:
        return None,None

    medianGr = ROOT.TGraphErrors()
    widthGr  = ROOT.TGraphErrors()

    xq = array('d', [0.16,0.5,0.84])
    yq = array('d', [0.0 ,0.0,0.0 ])

    for ybin in xrange(1,h.GetNbinsY()+1,step):

        tmp=h.ProjectionX('tmp',ybin,ybin+(step-1))
        tmp.GetQuantiles(3,yq,xq)
        ycen=h.GetYaxis().GetBinCenter(ybin)

        npts=medianGr.GetN()
        medianGr.SetPoint(npts,ycen,yq[1])
        medianGr.SetPointError(npts,0,1.2533*tmp.GetMeanError())
        widthGr.SetPoint(npts,ycen,yq[2]-yq[1])
        widthGr.SetPointError(npts,0,tmp.GetRMSError())

    return medianGr,widthGr

def getProfilePull(nGr,dGr):
    """build a pull out of two profiles"""

    if not nGr or not dGr:
        return None,None,None

    pullGr=ROOT.TGraphErrors()
    chi2=0
    x,n,d=ROOT.Double(0),ROOT.Double(0),ROOT.Double(0)
    for ipt in xrange(0,nGr.GetN()):
        nGr.GetPoint(ipt,x,n)
        en=nGr.GetErrorY(ipt)
        dGr.GetPoint(ipt,x,d)
        ed=dGr.GetErrorY(ipt)
        
        #diff=float(n)-float(d)
        #sigmaStat=ROOT.TMath.Sqrt(en**2+ed**2)
        #if sigmaStat==0: continue        
        #pull=diff/sigmaStat
        #chi2+=pull**2

        if float(d)==0: continue
        r=float(n)/float(d)
        rUnc=ROOT.TMath.Sqrt((en*float(d))**2+(ed*float(n))**2)/(float(d)**2)

        npt=pullGr.GetN()
        pullGr.SetPoint(npt,float(x),r)
        pullGr.SetPointError(npt,0,rUnc)

    scale=pullGr.GetMean(2)
    spread=pullGr.GetRMS(2)
 
    return pullGr,scale,spread

def getScatterFor(fIn,dirName):
    """builds the total data and total MC for a given distribution"""

    hData,hMC=None,None
    for key in fIn.Get(dirName).GetListOfKeys():
        h=key.ReadObj()
        if 'Data'==h.GetTitle():
            hData=h.Clone('data')
        else:
            if not hMC:
                hMC=h.Clone('mc')
                hMC.Reset('ICE')
            hMC.Add(h)
    return hData,hMC

def formatGraph(gr,color,marker,title,name=None):
    """format a graph"""
    if not gr: return
    gr.SetLineColor(color)
    gr.SetMarkerColor(color)
    gr.SetFillStyle(0)
    gr.SetMarkerStyle(marker)
    gr.SetTitle(title)
    if name: gr.SetName(name)

def getGraphsFor(fIn,dist,step):
    """get the formatted profiles and chi2 from the file"""
    graphs={}
    for era,color,marker in [('',ROOT.kBlack,20),
                             ('C',ROOT.kMagenta,22),
                             ('D',ROOT.kMagenta,23),
                             ('E',ROOT.kGreen+1,24),
                             ('F',ROOT.kGreen+1,25),
                             ('G',ROOT.kRed+1,21),
                             ('H',ROOT.kRed+1,26)]:
        hData, hMC    = getScatterFor(fIn,'z_2016{0}_{1}'.format(era,dist))
        dataProfile   = getProfiles(hData,step)
        mcProfile     = getProfiles(hMC,step)
        data2MC       = (getProfilePull(dataProfile[0],mcProfile[0]),
                         getProfilePull(dataProfile[1],mcProfile[1]))        
        for i in xrange(0,2):
            formatGraph(dataProfile[i],color,marker,'data','z_2016{0}_{1}_data_{2}'.format(era,dist,i))
            formatGraph(mcProfile[i],color,marker,'mc','z_2016{0}_{1}_mc_{2}'.format(era,dist,i))
            formatGraph(data2MC[i][0],color,marker,'2016'+era,'z_2016{0}_{1}_data2mc_{2}'.format(era,dist,i))
            
        graphs[era]={'data':dataProfile,'mc':mcProfile,'ratio':data2MC}
            
        #if hData : hData.Delete()
        #if hMC : hMC.Delete()

    return graphs
        

def parseDistTitle(dist):
    v,p=dist.split('_vs_')
    xtitle='Vertex multiplicity'
    if p=='pt' : xtitle='Dilepton transverse momentum [GeV]'
    if p=='mindz' : xtitle='#Delta z to closest vertex [cm]'
    if p=='vz' : xtitle='Primary vertex z [cm]'
    if p=='rho' : xtitle='Fastjet #rho'
    if p=='ht'  : xtitle='#Sigma |p_{T}| [GeV]'

    m='tk'
    if 'puppi' in v : m='puppi'
    if 'npv' in v :   m='npv'
    if 'ntnpv' in v : m='ntnpv'
    ytitle='h(%s) [GeV]'%m
    if '_sphericity' in v :  ytitle='h(%s) sphericity'%m
    if '_upar' in v : ytitle='u_{//}(%s) [GeV]'%m
    if '_uperp' in v : ytitle='u_{#perp}(%s) [GeV]'%m

    return xtitle,ytitle

def parseDistTitleForTable(dist):
    v,p=dist.split('_vs_')
    xtitle='$N_{vert}$'
    if p=='pt' : xtitle='$p_{T}(\\ell\\ell)$'
    if p=='mindz' : xtitle='$\\Delta$z'
    if p=='vz' : xtitle='$z_{PV}$'
    if p=='rho' : xtitle='$\\rho$'
    if p=='ht'  : xtitle='$\\Sigma |p_{T}|$'
    ytitle='h'
    if '_sphericity' in v :  ytitle='sphericity'
    if '_upar' in v : ytitle='$u_{//}$'
    if '_uperp' in v : ytitle='$u_{\\perp}$'

    return xtitle,ytitle


def main():

    url,out = sys.argv[1:3]
    metColl=['tkmet'] #,'puppimet','npvmet','ntnpv']
    metColl=['puppimet']
    metColl=['npvmet']
    metColl=['ntnpv']
    varColl=['pt','sphericity','upar','uperp']

    ROOT.gROOT.SetBatch(True)
    ROOT.gStyle.SetOptTitle(0)
    ROOT.gStyle.SetOptStat(0)

    c=ROOT.TCanvas('c','c',500,500)
    c.SetTopMargin(0)
    c.SetLeftMargin(0)
    c.SetRightMargin(0)
    c.SetBottomMargin(0)
    c.cd()
    p1=ROOT.TPad('p1','p1',0,0.5,1,1)
    p1.SetLeftMargin(0.12)
    p1.SetRightMargin(0.03)
    p1.SetTopMargin(0.1)
    p1.SetBottomMargin(0.01)
    p1.SetGridy()
    p1.SetGridx()
    p1.Draw()
    c.cd()
    p2=ROOT.TPad('p2','p2',0,0.0,1,0.5)
    p2.SetLeftMargin(0.12)
    p2.SetRightMargin(0.03)
    p2.SetTopMargin(0.01)
    p2.SetBottomMargin(0.2)
    p2.SetGridy()
    p2.SetGridx()
    p2.Draw()
    

    fIn=ROOT.TFile.Open(url)
    chi2Tables={'':{},'C':{},'D':{},'E':{},'F':{},'G':{},'H':{}}
    for m in metColl:
        for v in varColl : 
            for p,step in [('nvert',2),('ht',6),('pt',1),('mindz',1),('vz',2),('rho',2)]:
                dist='{0}_{1}_vs_{2}'.format(m,v,p)
                xtit,ytit=parseDistTitle(dist)

                graphs=getGraphsFor(fIn,dist,step)
                for i in xrange(0,2):
                    c.cd()
                    p1.cd()
                    p1.Clear()
                    leg1=ROOT.TLegend(0.15,0.88,0.3,0.75)
                    leg1.SetTextFont(42)
                    leg1.SetTextSize(0.06)
                    leg1.SetBorderSize(0)
                    leg1.SetFillStyle(0)
                    mg=ROOT.TMultiGraph()
                    for tag in ['mc','data']:
                        if not graphs[''][tag][i] : continue
                        if tag=='mc' : graphs[''][tag][i].SetMarkerStyle(24)
                        mg.Add(graphs[''][tag][i],'p')
                        leg1.AddEntry(graphs[''][tag][i],graphs[''][tag][i].GetTitle(),'ep')
                    mg.Draw('a')
                    leg1.Draw()
                    mg.GetYaxis().SetNdivisions(5)
                    mg.GetXaxis().SetTitleSize(0)
                    mg.GetXaxis().SetLabelSize(0)
                    mg.GetYaxis().SetTitleSize(0.06)
                    mg.GetYaxis().SetLabelSize(0.06)
                    mg.GetYaxis().SetTitle('{0} {1}'.format( 'Median' if i==0 else 'Width', ytit) )
                    txt=ROOT.TLatex()
                    txt.SetTextFont(42)
                    txt.SetNDC()
                    txt.SetTextSize(0.07)
                    txt.DrawLatex(0.12,0.94,'#bf{CMS} #it{preliminary}')
                    txt.DrawLatex(0.7,0.94,'35.9 fb^{-1} (13 TeV)')
                    
                    p2.cd()
                    p2.Clear()
                    rmg=ROOT.TMultiGraph()
                    leg=ROOT.TLegend(0.15,0.95,0.9,0.8)
                    leg.SetTextFont(42)
                    leg.SetTextSize(0.06)
                    leg.SetBorderSize(0)
                    leg.SetFillStyle(0)
                    leg.SetNColumns(4)
                    for era in ['C','D','E','F','G','H','']:
                        rmg.Add(graphs[era]['ratio'][i][0],'p')
                        leg.AddEntry(graphs[era]['ratio'][i][0],'2016'+era,'p')
                        
                        if not dist in chi2Tables[era]: chi2Tables[era][dist]=[]
                        chi2Tables[era][dist].append( graphs[era]['ratio'][i][1:3] )
                    rmg.Draw('a')
                    rmg.GetYaxis().SetNdivisions(5)
                    rmg.GetYaxis().SetRangeUser(0.65,1.45)
                    rmg.GetXaxis().SetTitleSize(0.06)
                    rmg.GetXaxis().SetLabelSize(0.06)
                    rmg.GetYaxis().SetTitleSize(0.06)
                    rmg.GetYaxis().SetLabelSize(0.06)
                    rmg.GetYaxis().SetTitle('data/MC') #(data-MC) / #sigma_{stat}')
                    rmg.GetXaxis().SetTitle(xtit)
                    leg.Draw()
                    c.cd()
                    c.Modified()
                    c.Update()
                    c.SaveAs(os.path.join(out,'%s_prof%d.png'%(dist,i)))
    fIn.Close()



    c=ROOT.TCanvas('c','c',500,1000)
    c.SetTopMargin(0)
    c.SetLeftMargin(0)
    c.SetRightMargin(0)
    c.SetBottomMargin(0)
    allPads=[]
    dy0=0.3
    for i in xrange(0,4):        
        ymin,ymax=0,dy0
        if i>0 : ymin,ymax=dy0+((1-dy0)/3.)*(i-1),dy0+((1-dy0)/3.)*i
        allPads.append( ROOT.TPad('p%d'%i,'p%d'%i,0.,ymin,1,ymax) )
        lm,tm,rm,bm=0.15,0.01,0.03,0.01
        if i==0: bm=0.35
        if i==3: tm=0.1
        allPads[-1].SetGridy()
        allPads[-1].SetLeftMargin(lm)
        allPads[-1].SetRightMargin(rm)
        allPads[-1].SetBottomMargin(bm)
        allPads[-1].SetTopMargin(tm)
        allPads[-1].Draw()


    for m in metColl:
        with open(os.path.join(out,'chi2summary_%s.dat'%m),'w') as table:
            table.write('\\hline\n')
            table.write('\\multirow{2}{*}{Distribution} & \multirow{2}{*}{Profile} & \\multicolumn{2}{c}{Median} & \\multicolumn{2}{c}{68\% width} \\\\\n')
            table.write('& & $\\mu$ & $\\sigma/\\mu$ & $\\mu$ & $\\sigma/\\mu$ & \\\\\hline\\hline\n')

            rows={}
            allSummaryGrs=[]
            for era in chi2Tables:
                summaryGrs=[ROOT.TGraph(),ROOT.TGraph(),ROOT.TGraph(),ROOT.TGraph()]
                color,marker = ROOT.kBlack,20
                if era=='C' : color,marker=ROOT.kMagenta,22
                if era=='D' : color,marker=ROOT.kMagenta,23
                if era=='E' : color,marker=ROOT.kGreen+1,24
                if era=='F' : color,marker=ROOT.kGreen+1,25
                if era=='G' : color,marker=ROOT.kRed+1,21
                if era=='H' : color,marker=ROOT.kRed+1,26                
                for i in xrange(0,len(summaryGrs)):
                    formatGraph(summaryGrs[i],color,marker,'2016'+era,'summary_%d_%s'%(i,era))
                table.write('\\multicolumn{5}{c}{\\bf2016%s}\\\\\n'%era)
                table.write('\\hline\n')
                for dist in chi2Tables[era]:
                    x,y=parseDistTitleForTable(dist)                    
                    scaleScale,scaleSpread = chi2Tables[era][dist][0]
                    widthScale,widthSpread = chi2Tables[era][dist][1]                    
                    table.write('%10s & %20s & %3.2f & %3.2f & %3.2f & %3.2f \\\\\n'% (y,x,scaleScale,scaleSpread,widthScale,widthSpread))

                    rowName=y+' vs '+x
                    if not rowName in rows: rows[rowName]=len(rows)
                    ix=rows[rowName]
                    ipt=summaryGrs[0].GetN()
                    summaryGrs[0].SetPoint(ipt,ix+0.5,scaleScale)
                    summaryGrs[1].SetPoint(ipt,ix+0.5,scaleSpread)
                    summaryGrs[2].SetPoint(ipt,ix+0.5,widthScale)
                    summaryGrs[3].SetPoint(ipt,ix+0.5,widthSpread)

                table.write('\\hline\n')
                allSummaryGrs.append( summaryGrs )

            allFrames=[]
            for i in xrange(0,4):
                allPads[i].cd()
                allPads[i].Clear()
                allFrames.append( ROOT.TH1F('frame%d'%i,'frame%d'%i,len(rows),0,len(rows) ) )
                if i==0:
                    for rowName in rows:
                        label=rowName
                        for tkn,rep in [('$',''),('\\','#'),('#ell','l')]:
                            label=label.replace(tkn,rep)                        
                        allFrames[-1].GetXaxis().SetBinLabel(rows[rowName]+1,label)                    
                    allFrames[-1].GetXaxis().LabelsOption("v")
                    allFrames[-1].GetXaxis().SetLabelSize(0.07)
                    allFrames[-1].GetXaxis().SetTitleSize(0.06)
                    allFrames[-1].GetYaxis().SetLabelSize(0.05)
                    allFrames[-1].GetYaxis().SetTitleSize(0.05)
                    allFrames[-1].GetXaxis().SetTitleOffset(3.0)
                    allFrames[-1].GetXaxis().SetTitle('Variable profile')
                else:
                    allFrames[-1].GetXaxis().SetLabelSize(0)
                    allFrames[-1].GetXaxis().SetTitleSize(0)
                    allFrames[-1].GetYaxis().SetLabelSize(0.06)
                    allFrames[-1].GetYaxis().SetTitleSize(0.06)
                allFrames[-1].GetYaxis().SetNdivisions(5)
                if i==0 : allFrames[-1].GetYaxis().SetTitle('Median scale')
                if i==1 : allFrames[-1].GetYaxis().SetTitle('Median spread')
                if i==2 : allFrames[-1].GetYaxis().SetTitle('68% width scale')
                if i==3 : allFrames[-1].GetYaxis().SetTitle('68% width spread')

                if i%2==0 :  allFrames[-1].GetYaxis().SetRangeUser(0.5,1.42)
                else      :  allFrames[-1].GetYaxis().SetRangeUser(0,0.42)
                allFrames[-1].Draw()
                leg=None
                if i==3:
                    leg=ROOT.TLegend(0.15,0.88,0.9,0.75)
                    leg.SetTextFont(42)
                    leg.SetTextSize(0.06)
                    leg.SetBorderSize(0)
                    leg.SetFillStyle(0)
                    leg.SetNColumns(4)

                for k in xrange(0,len(allSummaryGrs)):
                    allSummaryGrs[k][i].Draw('p')
                    if leg: leg.AddEntry(allSummaryGrs[k][i],allSummaryGrs[k][i].GetTitle(),'p')
                if i==3:
                    leg.Draw()
                    txt=ROOT.TLatex()
                    txt.SetTextFont(42)
                    txt.SetNDC()
                    txt.SetTextSize(0.07)
                    txt.DrawLatex(0.15,0.93,'#bf{CMS} #it{preliminary}')
                    txt.DrawLatex(0.7,0.93,'35.9 fb^{-1} (13 TeV)')
            c.cd()
            c.Modified()
            c.Update()
            c.SaveAs(os.path.join(out,'%s_profsummary.png'%(m)))
            

    print 'Plots and summary tables available in',out

if __name__ == "__main__":
    sys.exit(main())
