import ROOT
import os
import fnmatch

def getFromSemiParamReg(tree):
    """ loop over events and analyze the GD shape """

    w=ROOT.RooWorkspace('w')
    w.factory('lne1[-10,10]')
    w.factory('mu[-10,10]')
    w.factory('sigma[0,10]')
    w.factory("expr::t('(@0-@1)/@2',lne1,mu,sigma)")
    w.factory('aL[0.,10]')
    w.factory("expr::exp_L('@0<-@1 ? exp(@1*(@0+@1/2.)): 0.',t,aL)")
    w.factory('aR[0.,10]')
    w.factory("expr::exp_R('@0>@1 ? exp(-@1*(@0-@1/2.)): 0.',t,aR)")
    w.factory("expr::exp_0('@0>-@1 && @0<@2  ? exp(-@0*@0/2): 0.',t,aL,aR)")
    w.factory("EXPR::gdPDF('@0+@1+@2',exp_L,exp_0,exp_R)") 

    h=ROOT.TH1F('h','h',100,0,50)
    h.Sumw2()
    for i in xrange(0,tree.GetEntriesFast()):
        tree.GetEntry(i)

        #get the mean or these values
        #fixme when no loger shifted
        w.var('mu').setVal(tree.mu)
        w.var('sigma').setVal(abs(tree.sigma))
        w.var('aL').setVal(abs(tree.a1))
        w.var('aR').setVal(abs(tree.a2))
        mean=w.pdf('gdPDF').mean(w.var('lne1'))
        h.Fill(mean.getVal())
        mean.Delete()
        
    return h


ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)

HISTLIST=[ ('cat0', 'inclusive',         'isW>0'),
           ('cat1', 'sphericity>20%',    'isW>0 && tkmet_sphericity>0.20'),
           ('cat2', '5%<sphericity<20%', 'isW>0 && tkmet_sphericity>0.05 && tkmet_sphericity<0.20'),
           ('cat3', 'sphericity<5%',     'isW>0 && tkmet_sphericity<0.05'),
           ]
COLORS=[ROOT.kMagenta, ROOT.kMagenta+2, ROOT.kMagenta-9,ROOT.kRed+1,ROOT.kAzure+7, ROOT.kBlue-7]
LINES=[1,7]

#histos to show
histos={}

#base tree
print 'MC truth'
fBase=ROOT.TFile.Open('/eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest_regress/Chunks/WJetsToLNu_part1.root')
data=fBase.Get('data')
for hname,title,cut in HISTLIST:
    data.Draw('trueh>>hgen_%s(100,0,50)'%hname,cut,'goff')    
    if not hname in histos: histos[hname]=[]
    histos[hname].append( ROOT.gDirectory.Get('hgen_%s'%hname) )
    histos[hname][-1].SetDirectory(0)
    histos[hname][-1].SetTitle('Truth')
    histos[hname][-1].Scale(1./histos[hname][-1].Integral(0,51))
    histos[hname][-1].GetYaxis().SetTitle('PDF')
    histos[hname][-1].GetYaxis().SetRangeUser(0,0.08)
    histos[hname][-1].GetXaxis().SetTitle('W recoil [GeV]')
    histos[hname][-1].SetLineColor(ROOT.kBlack)
    histos[hname][-1].SetLineWidth(2)

#find all models with predictions
regDir='/eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest_scaleRegress/'
matches = []
for root, dirnames, filenames in os.walk(regDir):
    for filename in fnmatch.filter(filenames, 'WJetsToLNu_part1_predict.root'):
        url=os.path.join(root, filename)
        model=root.split('/')[-2].split('_')[-1]
        matches.append((model,url))

#project regressed
for model,url in matches:
    print model,url
    f=ROOT.TFile.Open(url)
    pred=f.Get('tree')
    pred.AddFriend(data)
    for hname,_,cut in HISTLIST:
        
        #try:
        #    h=getFromSemiParamReg(pred)
        #    histos[hname].append( h.Clone('h_%s_%s'%(hname,model)) )
        #    h.Delete()
        #except:
        pred.Draw('TMath::Exp(mu)*tkmet_pt>>h_%s_%s(100,0,50)'%(hname,model),cut,'goff')        
        histos[hname].append( ROOT.gDirectory.Get('h_%s_%s'%(hname,model)) )
        histos[hname][-1].SetDirectory(0)
        histos[hname][-1].SetTitle('Model %s'%model)
        histos[hname][-1].Scale(1./histos[hname][-1].Integral(0,51))
        histos[hname][-1].SetLineWidth(2)

        idx=len(histos[hname])-1        
        histos[hname][-1].SetLineColor(COLORS[idx%len(COLORS)])
        histos[hname][-1].SetLineStyle(LINES[idx/len(COLORS)])
    f.Close()


fBase.Close()

# show the plots
c=ROOT.TCanvas('c','c',500,500)
c.SetTopMargin(0.05)
c.SetBottomMargin(0.1)
c.SetLeftMargin(0.12)
c.SetRightMargin(0.03)
for hname,title,_ in HISTLIST:
    
    drawOpt='hist'
    for h in histos[hname]: 
        h.Draw(drawOpt)
        drawOpt='histsame'

    leg=c.BuildLegend(0.65,0.9,0.9,0.03*len(histos[hname]))
    leg.SetTextFont(42)
    leg.SetTextSize(0.036)
    leg.SetBorderSize(0)
    leg.SetFillStyle(0)
    
    label = ROOT.TLatex()
    label.SetNDC()
    label.SetTextFont(42)
    label.SetTextSize(0.036)
    label.DrawLatex(0.12,0.96,'#bf{CMS} #it{Simulation Preliminary}   W#rightarrowl#nu (13 TeV)') 
    label.DrawLatex(0.65,0.9,title)

    c.Modified()
    c.Update()
    c.SaveAs('%s.png'%hname)
    raw_input()
