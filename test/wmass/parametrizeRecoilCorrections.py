import ROOT

def showFitResults(var,data,pdf,title,rangeX=[-6,6],nbins=50,name='fitresults',extraText=[]):
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetOptTitle(0)

    c=ROOT.TCanvas('c','c',500,500)
    c.SetTopMargin(0)
    c.SetLeftMargin(0)
    c.SetRightMargin(0)
    c.SetBottomMargin(0)
    p1 = ROOT.TPad('p1','p1',0.0,0.25,1.0,1.0)
    p1.SetRightMargin(0.05)
    p1.SetLeftMargin(0.15)
    p1.SetTopMargin(0.08)
    p1.SetBottomMargin(0.02)
    p1.Draw()
    c.cd()
    p2 = ROOT.TPad('p2','p2',0.0,0.0,1.0,0.25)
    p2.SetBottomMargin(0.45)
    p2.SetRightMargin(0.05)
    p2.SetLeftMargin(0.15)
    p2.SetTopMargin(0.001)
    p2.SetGridy(True)
    p2.Draw()

    p1.cd()
    frame=var.frame(ROOT.RooFit.Range(rangeX[0],rangeX[1]),ROOT.RooFit.Bins(50))
    data.plotOn(frame,ROOT.RooFit.Name('datafit'),ROOT.RooFit.DrawOption('p'))

    pdf.plotOn(frame,               
               ROOT.RooFit.Name('pdf'),
               ROOT.RooFit.ProjWData(data),
               ROOT.RooFit.MoveToBack())
    totalchisq=frame.chiSquare()    
    frame.Draw()
    frame.GetYaxis().SetRangeUser(0,frame.GetMaximum()*1.2)
    frame.GetYaxis().SetTitle("Events")
    frame.GetYaxis().SetTitleOffset(0.8)
    frame.GetYaxis().SetTitleSize(0.07)
    frame.GetYaxis().SetLabelSize(0.05)
    frame.GetXaxis().SetTitleSize(0.)
    frame.GetXaxis().SetLabelSize(0.)
    frame.GetXaxis().SetTitleOffset(1.5)
    frame.GetXaxis().SetTitle(var.GetTitle())
    
    label = ROOT.TLatex()
    label.SetNDC()
    label.SetTextFont(42)
    label.SetTextSize(0.045)
    label.DrawLatex(0.18,0.85,'#scale[1.2]{#bf{CMS} #it{Simulation Preliminary}}') 
    label.DrawLatex(0.85,0.95,'(13 TeV)')
    label.DrawLatex(0.18,0.8,'%s'%title)
    label.DrawLatex(0.18,0.75,'#chi^{2}/dof = %3.1f'%totalchisq)
    for i in xrange(0,len(extraText)):
        label.DrawLatex(0.18,0.7-0.05*i,extraText[i])
    p1.RedrawAxis()

    #show pull
    p2.cd()
    hpull = frame.pullHist('datafit','pdf')
    pullFrame=var.frame(ROOT.RooFit.Range(rangeX[0],rangeX[1]),ROOT.RooFit.Bins(nbins))
    pullFrame.addPlotable(hpull,"P") ;
    pullFrame.Draw()
    pullFrame.GetYaxis().SetTitle("#frac{Data-Fit}{Unc.}")
    pullFrame.GetYaxis().SetTitleSize(0.20)
    pullFrame.GetYaxis().SetLabelSize(0.16)
    pullFrame.GetYaxis().SetTitleOffset(0.27)
    pullFrame.GetXaxis().SetTitleSize(0.20)
    pullFrame.GetXaxis().SetLabelSize(0.16)
    pullFrame.GetXaxis().SetTitleOffset(0.85)
    pullFrame.GetYaxis().SetNdivisions(4)
    pullFrame.GetYaxis().SetRangeUser(-8.0,8.0)
    p2.RedrawAxis()
        
    c.cd()
    c.Modified()
    c.Update()
    c.SaveAs(name+'.png')


url='/eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest_regress/Chunks/WJetsToLNu_part1.root'

grEvol={}
varsToProfile={
    'tkmet_sphericity':[(0,0.05),(0.05,0.10),(0.10,0.20),(0.20,0.30),(0.30,0.45),(0.45,0.60),(0.60,0.8),(0.8,1.0)],
    'tkmet_pt':[(0,10),(10,20),(20,30),(30,50),(50,100)]
    }
varTitles={
    'tkmet_sphericity':'S',
    'tkmet_pt':'h_{tk}'
}     
parTitles={'mu':'#mu','sigma':'#sigma','aL':'a_{L}','aR':'a_{R}',
           'mu_e2':'#mu','sigma_e2':'#sigma','aL_e2':'a_{L}','aR_e2':'a_{R}','off_e2':'offset'}


#import data to fit
f=ROOT.TFile.Open(url)
tree=f.Get('data')
w=ROOT.RooWorkspace('w')
w.factory('lne1[-10,10]')
w.var('lne1').SetTitle('log(e_{1})=log(h_{true}/h_{tk})')
w.factory('e2[-3.15,3.15]')
w.var('e2').SetTitle('e_{2}=#phi_{true}-#phi_{tk}')
w.factory('tkmet_sphericity[0,1]')
w.factory('tkmet_pt[0,100]')
args=ROOT.RooArgSet(w.var('lne1'),w.var('e2'),w.var('tkmet_sphericity'),w.var('tkmet_pt'))
data=ROOT.RooDataSet('data','data',tree,args) 
getattr(w,'import')(data)


#define the pdf for the scale
mu=data.mean(w.var('lne1'))
sigma=data.sigma(w.var('lne1'))
w.factory('mu[%f,%f,%f]'%(mu,mu*0.5,mu*10))
w.factory('sigma[%f,%f,%f]'%(sigma,sigma*0.5,sigma*2))
w.factory("expr::t('(@0-@1)/@2',lne1,mu,sigma)")
w.factory('aL[1,0.1,10]')
w.factory("expr::exp_L('@0<-@1 ? exp(@1*(@0+@1/2.)): 0.',t,aL)")
w.factory('aR[1,0.1,10]')
w.factory("expr::exp_R('@0>@1 ? exp(-@1*(@0-@1/2.)): 0.',t,aR)")
w.factory("expr::exp_0('@0>-@1 && @0<@2  ? exp(-@0*@0/2): 0.',t,aL,aR)")
w.factory("EXPR::gdPDF('@0+@1+@2',exp_L,exp_0,exp_R)") 

#define the pdf for the regression
sigma=data.sigma(w.var('e2'))
w.factory('off_e2[0,0,0.5]')
w.factory('mu_e2[0,-0.5,0.5]')
w.factory('sigma_e2[%f,%f,%f]'%(sigma,sigma*0.5,sigma*2))
w.factory("expr::t_e2('(@0-@1)/@2',e2,mu_e2,sigma_e2)")
w.factory('aL_e2[1,0.1,10]')
w.factory("expr::exp_L_e2('@0<-@1 ? exp(@1*(@0+@1/2.))+@2: 0.',t_e2,aL_e2,off_e2)")
w.factory('aR_e2[1,0.1,10]')
w.factory("expr::exp_R_e2('@0>@1 ? exp(-@1*(@0-@1/2.))+@2: 0.',t_e2,aR_e2,off_e2)")
w.factory("expr::exp_0_e2('@0>-@1 && @0<@2  ? exp(-@0*@0/2)+@3 : 0.',t_e2,aL_e2,aR_e2,off_e2)")
w.factory("EXPR::gdoffPDF('@0+@1+@2',exp_L_e2,exp_0_e2,exp_R_e2)") 

ROOT.gROOT.SetBatch(True)
for var in varsToProfile:

    for varMin,varMax in varsToProfile[var]:

        #init profile graph
        if not var in grEvol:
            grEvol[var]=[ROOT.TGraphErrors() for i in xrange(5)]

        data=w.data('data').reduce('{0}>={1} && {0}<{2}'.format(var,varMin,varMax))
        w.var('mu').setVal( data.mean(w.var('lne1')) )
        w.var('sigma').setVal( data.sigma(w.var('lne1')) )    
        w.var('sigma').setRange(  w.var('sigma').getVal()*0.5, w.var('sigma').getVal())
        w.var('mu_e2').setVal( data.mean(w.var('e2')) )
        w.var('sigma_e2').setVal( data.sigma(w.var('e2')) )    
        w.var('sigma_e2').setRange( w.var('sigma_e2').getVal()*0.5,w.var('sigma_e2').getVal() )

        pdfName='gdoffPDF'
        regTarget='e2'
        pdfName='gdPDF'
        regTarget='lne1'
        w.pdf(pdfName).fitTo( data )

        extraText=[]
        iter = w.pdf(pdfName).getParameters(data).createIterator()
        iparam = iter.Next()
        while iparam :
            if not iparam.getAttribute('Constant'):
                icount=len(extraText)
                np=grEvol[var][icount].GetN()
                grEvol[var][icount].SetPoint(np,0.5*(varMax+varMin),iparam.getVal())
                grEvol[var][icount].SetPointError(np,0.5*(varMax-varMin),iparam.getError())
                grEvol[var][icount].SetTitle(parTitles[iparam.GetName()])
                grEvol[var][icount].SetName('%s_prof_%s'%(iparam.GetName(),var))
                extraText.append('#scale[0.6]{%s=%3.2f#pm%3.2f}'%(iparam.GetName(),
                                                                  iparam.getVal(),
                                                                  iparam.getError()))
                iparam = iter.Next()
        

        title='%3.2f<%s<%3.2f'%(varMin,varTitles[var],varMax)
        name='%s_%s_%3.2fto%3.2f'%(regTarget,var,varMin,varMax)
        showFitResults(var=w.var(regTarget),data=data,pdf=w.pdf(pdfName),title=title,rangeX=[-6,6],nbins=50,name=name,extraText=extraText)

ROOT.gROOT.SetBatch(False)
c=ROOT.TCanvas('c','c',500,500)
c.SetTopMargin(0.05)
c.SetLeftMargin(0.12)
c.SetRightMargin(0.05)
c.SetBottomMargin(0.1)
for var in grEvol:
    for gr in grEvol[var]:
        if gr.GetN()==0 : continue
        gr.SetMarkerStyle(20)
        gr.Draw('ap')
        gr.GetXaxis().SetTitle(varTitles[var])
        gr.GetYaxis().SetTitle(gr.GetTitle())
        gr.GetYaxis().SetTitleSize(0.04)
        gr.GetXaxis().SetTitleSize(0.04)
        label = ROOT.TLatex()
        label.SetNDC()
        label.SetTextFont(42)
        label.SetTextSize(0.04)
        label.DrawLatex(0.15,0.9,'#bf{CMS} #it{Simulation Preliminary}') 
        label.DrawLatex(0.8,0.95,'#scale[0.9]{(13 TeV)}')
        c.Modified()
        c.Update()
        c.SaveAs('%s.png'%gr.GetName())
