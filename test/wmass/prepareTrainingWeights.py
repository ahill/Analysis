import ROOT
import sys
import numpy as np
from array import array
import pickle
from UserCode.DataAnalysis.UniformRejectionSampling import *


#define input files
urlList=['/eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest_regress/Chunks/WJetsToLNu_part1.root']
if len(sys.argv)>1: urlList=sys.argv[1].split(',')
print 'Will use',urlList

#define output
outf='data/wmass/trainweights.pck'

#read tree
tree=ROOT.TChain('data')
for url in urlList: tree.AddFile(url)
varList=['lne1','e2','tkmet_logpt','tkmet_sphericity','trueh']
var=[]
nentries=tree.GetEntries()
for i in xrange(0,nentries):
    tree.GetEntry(i)
    var.append( [ getattr(tree,v) for v in varList] )

#compute the CDF from the quantiles
var=np.array(var)
q=[x for x in range(0,101)]
yq=np.percentile(var,q,axis=0)

#save quantiles and PDF for weighting
pdfcdfMap={}
for i in xrange(0,len(varList)):
    pdf=ROOT.TH1F('pdf_%s'%varList[i],';%s;PDF'%varList[i],99,array('d',yq[:,i]))
    pdf.Sumw2()
    for x in var[:,i]: 
        ibin=pdf.GetXaxis().FindBin(x)
        w=1./(pdf.GetXaxis().GetBinWidth(ibin)*nentries)
        pdf.Fill(x,w)
    cdf=ROOT.TGraph()
    cdf.SetName('cdf_%s'%varList[i])
    for x in yq[:,i]:
        cdf.SetPoint(cdf.GetN(),x,cdf.GetN())
    pdfcdfMap[varList[i]]=(pdf,cdf,yq[:,i])
with open(outf, 'w') as cachefile:
    pickle.dump(pdfcdfMap,cachefile, pickle.HIGHEST_PROTOCOL)

#start the uniform rejection sampling method
urs=UniformRejectionSampling(outf)
urs.defineAcceptanceRanges(5,95)

#some control plots
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)
c=ROOT.TCanvas('c','c',500,500)
c.SetTopMargin(0.05)
c.SetBottomMargin(0.1)
c.SetRightMargin(0.05)
c.SetLeftMargin(0.12)
for i in xrange(0,len(varList)):

    pdf,cdf,_=pdfcdfMap[varList[i]]
    pdf.SetTitle('PDF')

    pdfw=pdf.Clone('pdfuniw')
    pdfw.Reset('ICE')
    pdfw.SetTitle('Quantile morphed')
    #pdf to uniform by inverse quantiles
    a,b=yq[:,i][0],yq[:,i][-1]
    for x in var[:,i]:
        xq=cdf.Eval(x)/100.
        u=(b-a)*xq+a
        ibin=pdf.GetXaxis().FindBin(u)
        w=1./(pdf.GetXaxis().GetBinWidth(ibin)*nentries)
        pdfw.Fill(u,w)

    #pdf to uniform by sampling
    nacc=0
    pdfs=pdf.Clone('pdfunis')
    pdfs.SetTitle('sampled')
    pdfs.Reset('ICE')
    for x in var[:,i]:        
        if not urs.accept(x,varList[i]) : continue
        ibin=pdf.GetXaxis().FindBin(x)
        w=1./pdf.GetXaxis().GetBinWidth(ibin)
        pdfs.Fill(x,w)
        nacc+=1
    pdfs.Scale(1./nacc)

    print 'Efficiency for ',varList[i],' is ',float(nacc)/float(nentries)

    pdf.Draw()
    pdfw.SetLineColor(ROOT.kRed)
    pdfw.Draw('same')
    pdfs.SetLineColor(ROOT.kGreen)
    pdfs.Draw('e1same')
    leg=c.BuildLegend(0.15,0.9,0.4,0.8)
    leg.SetBorderSize(0)
    leg.SetFillStyle(0)
    leg.SetTextFont(42)
    leg.SetTextSize(0.04)
    c.Modified()
    c.Update()
    c.SaveAs(varList[i]+'.png')
