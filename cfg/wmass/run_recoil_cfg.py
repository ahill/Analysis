##########################################################
##       CONFIGURATION FOR RECOIL ANALYSIS               #
##########################################################
import PhysicsTools.HeppyCore.framework.config as cfg
import re

#-------- LOAD ALL ANALYZERS -----------
from CMGTools.WMass.analyzers.dmCore_modules_cff import *
from PhysicsTools.HeppyCore.framework.heppy_loop import getHeppyOption
from CMGTools.WMass.analyzers.treeProducerWMass import * 

# tree producer
treeProducer = cfg.Analyzer(AutoFillTreeProducer, 
                            name='treeProducerWMass',
                            vectorTree = True,
                            saveTLorentzVectors = False,  
                            defaultFloatType = 'F', 
                            PDFWeights = PDFWeights,
                            globalVariables = wmass_globalVariables+pdfsVariables+wmass_vertexVariables,
                            globalObjects = wmass_globalObjects,
                            collections = wmass_collections
                            )

#remove un-needed
for m in [photonAna]: dmCoreSequence.remove(m)

#-------- SET OPTIONS AND REDEFINE CONFIGURATIONS -----------

runData = getHeppyOption("runData",False)
forcedSplitFactor = getHeppyOption("splitFactor",-1)
forcedFineSplitFactor = getHeppyOption("fineSplitFactor",-1)
isTest = getHeppyOption("test",None) != None and not re.match("^\d+$",getHeppyOption("test"))
selectedEvents=getHeppyOption("selectEvents","")
keepLHEweights = True
dataEras=getHeppyOption("dataEras",'CDEFGH')

#---------- LEPTONS ---------------------------------------

# lepton scale / resolution corrections
def doECalElectronCorrections(sync=False,era="25ns"):
    global lepAna, monoJetCtrlLepSkim
    lepAna.doElectronScaleCorrections = {
        'data' : 'EgammaAnalysis/ElectronTools/data/ScalesSmearings/Winter_2016_reReco_v1_ele',
        'GBRForest': ('$CMSSW_BASE/src/CMGTools/RootTools/data/egamma_epComb_GBRForest_76X.root',
                      'gedelectron_p4combination_'+era),
        'isSync': sync
    }
def doECalPhotonCorrections(sync=False):
    global photonAna, gammaJetCtrlSkimmer
    photonAna.doPhotonScaleCorrections = {
        'data' : 'EgammaAnalysis/ElectronTools/data/ScalesSmearings/80X_ichepV2_2016_pho',
        'isSync': sync
    }
def doKalmanMuonCorrections(sync=False,smear="basic"):
    global lepAna
    lepAna.doMuonScaleCorrections = ( 'Kalman', {
        'MC': 'MC_80X_13TeV',
        'Data': 'DATA_80X_13TeV',
        'isSync': sync,
        'smearMode':smear
    })

lepAna.doMiniIsolation = True
lepAna.packedCandidates = 'packedPFCandidates'
lepAna.miniIsolationPUCorr = 'rhoArea'
lepAna.miniIsolationVetoLeptons = None
lepAna.doIsolationScan = False
lepAna.loose_electron_id = "POG_Cuts_ID_SPRING16_25ns_v1_HLT"
lepAna.loose_muon_isoCut     = lambda muon : True
lepAna.loose_electron_isoCut = lambda elec : True

# skimming
ttHLepSkim.minLeptons = 1
ttHLepSkim.maxLeptons = 999
ttHLepSkim.ptCuts = [15]

#---------- JETS ---------------------------------------
jetAna.lepSelCut = lambda lep : False # no cleaning of jets with leptons
jetAna.copyJetsByValue = True # do not remove this
metAna.copyMETsByValue = True # do not remove this
jetAna.doQG = True
jetAna.calculateType1METCorrection = True

#---------- MET ------------------------

#additional filters
from CMGTools.TTHAnalysis.analyzers.hbheAnalyzer import hbheAnalyzer
hbheAna = cfg.Analyzer(
    hbheAnalyzer, name="hbheAnalyzer", IgnoreTS4TS5ifJetInLowBVRegion=False
    )
dmCoreSequence.insert(-1,hbheAna)
treeProducer.globalVariables.append(NTupleVariable("hbheFilterNew50ns", lambda ev: ev.hbheFilterNew50ns, int, help="new HBHE filter for 50 ns"))
treeProducer.globalVariables.append(NTupleVariable("hbheFilterNew25ns", lambda ev: ev.hbheFilterNew25ns, int, help="new HBHE filter for 25 ns"))
treeProducer.globalVariables.append(NTupleVariable("hbheFilterIso", lambda ev: ev.hbheFilterIso, int, help="HBHE iso-based noise filter"))
treeProducer.globalVariables.append(NTupleVariable("Flag_badChargedHadronFilter", lambda ev: ev.badChargedHadron, help="bad charged hadron filter decision"))
treeProducer.globalVariables.append(NTupleVariable("Flag_badMuonFilter", lambda ev: ev.badMuon, help="bad muon filter decision"))

# additional MET quantities
metAna.doneutrals=True
#metAna.useLeptonPV=True
metAna.recalibrate = "type1"
treeProducer.globalVariables.append(NTupleVariable("met_trkPt", lambda ev : ev.tkMet.pt() if  hasattr(ev,'tkMet') else  0, help="tkmet p_{T}"))
treeProducer.globalVariables.append(NTupleVariable("met_trkPhi", lambda ev : ev.tkMet.phi() if  hasattr(ev,'tkMet') else  0, help="tkmet phi"))

# add puppi met
puppiMetAna=metAna.clone( name='pupimetAnalyzer',
                          metCollection='slimmedMETsPuppi',
                          noPUMetCollection='slimmedMETsPuppi',
                          doTkMet=False,
                          includeTkMetCHS=False,
                          includeTkMetPVTight=False,
                          doMetNoPU=False,
                          storePuppiExtra=False,
                          collectionPostFix='puppi')


## histo counter
dmCoreSequence.insert(dmCoreSequence.index(skimAnalyzer), histoCounter)

# gen level information
if not runData:
    genAna.saveAllInterestingGenParticles = True

                                         
#-------- SAMPLES AND TRIGGERS -----------
from CMGTools.RootTools.samples.triggers_13TeV_DATA2016 import *
triggerFlagsAna.triggerBits = {'DoubleMu' : triggers_mumu_iso,
                               'DoubleMuSS' : triggers_mumu_ss,
                               'DoubleMuNoIso' : triggers_mumu_noniso,
                               'DoubleEl' : triggers_ee,
                               'MuEG'     : triggers_mue,
                               'SingleMu' : triggers_1mu_iso,
                               'SingleEl'     : triggers_1e,
                               }
triggerFlagsAna.unrollbits = True
triggerFlagsAna.saveIsUnprescaled = True
triggerFlagsAna.checkL1Prescale = True

from CMGTools.RootTools.samples.samples_13TeV_RunIISummer16MiniAODv2 import *
from CMGTools.RootTools.samples.samples_13TeV_DATA2016 import *
from CMGTools.HToZZ4L.tools.configTools import printSummary, configureSplittingFromTime, cropToLumi, prescaleComponents, insertEventSelector, mergeExtensions
from CMGTools.RootTools.samples.autoAAAconfig import *

if not runData:
    selectedComponents = [DYJetsToLL_M50, WJetsToLNu, WWTo2L2Nu,WWToLNuQQ] # + [TT_pow, T_tch_powheg, TBar_tch_powheg, WZTo1L3Nu,ZZTo2L2Nu,ZZTo2L2Q]
    configureSplittingFromTime(selectedComponents,30,1)
else:
    dataChunks = []
    json = '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions16/13TeV/ReReco/Final/Cert_271036-284044_13TeV_23Sep2016ReReco_Collisions16_JSON.txt' # 36.5/fb
    run_ranges = []; useAAA=False; 
    for era in dataEras:
        processing = "Run2016%s-18Apr2017-v1" % era; short = "Run2016%s" % era; dataChunks.append((json,processing,short,run_ranges,useAAA))
    
    DatasetsAndTriggers = []
    selectedComponents = [];
    exclusiveDatasets = False; 
    DatasetsAndTriggers.append( ("SingleMuon", triggers_1mu_iso + triggers_1mu_noniso) )
    for json,processing,short,run_ranges,useAAA in dataChunks:
        if len(run_ranges)==0: run_ranges=[None]
        vetos = []
        for pd,triggers in DatasetsAndTriggers:
            for run_range in run_ranges:
                label = ""
                if run_range!=None:
                    label = "_runs_%d_%d" % run_range if run_range[0] != run_range[1] else "run_%d" % (run_range[0],)
                compname = pd+"_"+short+label
                if pd=='SingleMuon' and 'Run2016F-18Apr2017-v1' in processing: processing='Run2016F-18Apr2017-v2'
                comp = kreator.makeDataComponent(compname, 
                                                 "/"+pd+"/"+processing+"/MINIAOD",
                                                 "CMS", ".*root", 
                                                 json=json, 
                                                 run_range=(run_range if "PromptReco" not in processing else None), 
                                                 triggers=triggers[:], vetoTriggers = vetos[:],
                                                 useAAA=useAAA)
                if "PromptReco" in processing:
                    from CMGTools.Production.promptRecoRunRangeFilter import filterComponent
                    filterComponent(comp, verbose=1)
                print "Will process %s (%d files)" % (comp.name, len(comp.files))
                comp.splitFactor = len(comp.files)/4
                comp.fineSplitFactor = 1
                selectedComponents.append( comp )
            if exclusiveDatasets: vetos += triggers

    configureSplittingFromTime(selectedComponents,30,2)

#-------- SEQUENCE -----------

sequence = cfg.Sequence(dmCoreSequence+[puppiMetAna,treeProducer,])
preprocessor = None

#-------- HOW TO RUN -----------

test = getHeppyOption('test')
if test == 'mc' or test=='data':
    if test=='mc':
        comp = WJetsToLNu_LO
        comp.files = ['/eos/cms/store/cmst3/user/psilva/Wmass/WJetsMG_test/0A85AA82-45BB-E611-8ACD-001E674FB063-9552f253c2fa2ae.root']   
    else:
        comp=SingleMuon_Run2016B_18Apr2017
        comp.files=['/eos/cms/store/data/Run2016G/SingleMuon/MINIAOD/18Apr2017-v1/120000/1C169863-7541-E711-81BD-1CC1DE1CEFE0.root']
    print comp.files
    comp.splitFactor = 1
    comp.fineSplitFactor = 1
    selectedComponents = [ comp ]


## Auto-AAA
if not getHeppyOption("isCrab"): autoAAA(selectedComponents)

## output service
outputService=[]
from PhysicsTools.HeppyCore.framework.services.tfile import TFileService
output_service = cfg.Service(TFileService,
                             'outputfile',
                             name="outputfile",
                             fname='treeProducerWMass/tree.root',
                             option='recreate'
                             )
outputService.append(output_service)

# print summary of components to process
printSummary(selectedComponents)

# the following is declared in case this cfg is used in input to the heppy.py script
from PhysicsTools.HeppyCore.framework.eventsfwlite import Events
from CMGTools.TTHAnalysis.tools.EOSEventsWithDownload import EOSEventsWithDownload
event_class = EOSEventsWithDownload if not preprocessor else Events
EOSEventsWithDownload.aggressive = 2 # always fetch if running on Wigner
if getHeppyOption("nofetch") or getHeppyOption("isCrab"):
    event_class = Events
    if preprocessor: preprocessor.prefetch = False
config = cfg.Config( components = selectedComponents,
                     sequence = sequence,
                     services = outputService, 
                     preprocessor = preprocessor, 
                     events_class = event_class)
