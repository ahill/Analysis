import optparse
import os,sys
import json
import commands
import ROOT
import ast

"""
steer the script
"""
def main():

    #configuration
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage)
    parser.add_option('--json',      dest='inJson'  ,      help='json file with processed runs [%default]',          
                      default='/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions16/13TeV/ReReco/Final/Cert_271036-284044_13TeV_23Sep2016ReReco_Collisions16_JSON.txt',
                      type='string')
    parser.add_option('--rr',        dest='rr'  ,          help='run ranges "{\'tag\':[ri,rf];\'tag\':[ri,rf],...}" [%default]',  
                      default="{'2016C':[275656,276283],'2016D':[276315,276811],'2016E':[276831,277420],'2016F':[277932,278808],'2016G':[278820,280384],'2016H':[281613,284044]}",    
                      type='string')
    parser.add_option('--out',       dest='output'  ,      help='output file with pileup distributions[%default]',  default='data/wmass/pileup.root',    type='string')
    parser.add_option('--mbXsec',    dest='mbXsec'  ,      help='minimum bias cross section to use [%default]',     default=69200,   type=float)
    parser.add_option('--nbins',     dest='nbins'  ,       help='number of bins [%default]',     default=100,   type=float)
    parser.add_option('--mbXsecUnc', dest='mbXsecUnc'  ,   help='minimum bias cross section unc. [%default]',       default=0.047,   type=float)
    parser.add_option('--puJson',    dest='puJson'  ,      help='pileup json file [%default]',      
                      default='/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions16/13TeV/PileUp/pileup_latest.txt',
                      type='string')
    (opt, args) = parser.parse_args()
    
    jsons={'':opt.inJson}
    if opt.rr:
        print 'Refining json file'
        rr=ast.literal_eval(opt.rr)
        for key in rr:
            print key, rr[key]
            os.system('filterJSON.py --min {0} --max {1} --output {2}.json {3}'.format(rr[key][0],rr[key][1],key,opt.inJson))
            jsons[key]='%s.json'%key

    #compute pileup in data assuming different xsec
    MINBIASXSEC={'nom':opt.mbXsec,'up':opt.mbXsec*(1+opt.mbXsecUnc),'down':opt.mbXsec*(1-opt.mbXsecUnc)}
    puDist={}
    for key in jsons:
        print 'Computing pileup',key
        puDist[key]=[]
        for scenario in MINBIASXSEC:
            print scenario, 'xsec=',MINBIASXSEC[scenario]
            cmd='pileupCalc.py --calcMode tru -i %s --inputLumiJSON %s --calcMode true --minBiasXsec %f --maxPileupBin %d --numPileupBins %d Pileup.root'%(jsons[key],opt.puJson,MINBIASXSEC[scenario],opt.nbins,opt.nbins)
            commands.getstatusoutput(cmd)

            fIn=ROOT.TFile.Open('Pileup.root')
            puDist[key].append( fIn.Get('pileup').Clone('pu_'+scenario ) )
            puDist[key][-1].SetDirectory(0)
            fIn.Close()


    #save pileup weights to file
    print 'Saving results to',opt.output
    fOut=ROOT.TFile.Open(opt.output,'RECREATE')
    for key in puDist:
        fOut.cd()
        outDir=fOut
        if len(key)>0: 
            outDir=fOut.mkdir(key)
            outDir.cd()
        for gr in puDist[key]: 
            gr.SetDirectory(outDir)
            gr.Write()
    fOut.Close()

"""
for execution from another script
"""
if __name__ == "__main__":
    sys.exit(main())
