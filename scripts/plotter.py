import optparse
import os,sys
import json
import pickle
from collections import OrderedDict

from UserCode.DataAnalysis.Plot import *

"""
steer the script
"""
def main():

    #configuration
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage)
    parser.add_option(     '--mcUnc',         dest='mcUnc'  ,      help='common MC related uncertainty (e.g. lumi)',        default=0,              type=float)
    parser.add_option(     '--com',           dest='com'  ,        help='center of mass energy',                            default='13 TeV',       type='string')
    parser.add_option('-j', '--json',         dest='json'  ,       help='json with list of files',        default=None,              type='string')
    parser.add_option( '--systJson',          dest='systJson',     help='json with list of systematics',  default=None, type='string')
    parser.add_option(      '--signalJson',   dest='signalJson',   help='signal json list',               default=None,              type='string')
    parser.add_option('-i', '--inDir',        dest='inDir' ,       help='input directory',                default=None,              type='string')
    parser.add_option('-O', '--outDir',       dest='outDir' ,      help='output directory',               default=None,             type='string')
    parser.add_option('-o', '--outName',      dest='outName' ,     help='name of the output file',        default='plotter.root',    type='string')
    parser.add_option(      '--cmsLabel',     dest='cmsLabel' ,    help='cms label',                      default='#bf{CMS} #it{preliminary}',  type='string')
    parser.add_option(      '--formats',      dest='formats' ,     help='plot formats [%default]',        default='pdf,png',         type='string')
    parser.add_option(      '--noStack',      dest='noStack',      help='don\'t stack distributions',     default=False,             action='store_true')
    parser.add_option(      '--noRatio',      dest='noRatio',      help='do not show ratio',              default=False,             action='store_true')
    parser.add_option(      '--noUncs',       dest='noUncs',       help='no uncertainties',               default=False,             action='store_true')
    parser.add_option(      '--doDataOverMC', dest='doDataOverMC', help='do data/MC ratio',               default=False,             action='store_true')
    parser.add_option(      '--saveLog',      dest='saveLog' ,     help='save log versions of the plots', default=False,             action='store_true')
    parser.add_option(      '--scaleToData',  dest='scaleToData' , help='scale total MC to data',         default=False,             action='store_true')
    parser.add_option(      '--silent',       dest='silent' ,      help='only dump to ROOT file',         default=False,             action='store_true')
    parser.add_option(      '--onlyData',     dest='onlyData' ,    help='only plots containing data',     default=False,             action='store_true')
    parser.add_option(      '--saveTeX',      dest='saveTeX' ,     help='save as tex file as well',       default=False,             action='store_true')
    parser.add_option(      '--rebin',        dest='rebin',        help='rebin factor',                   default=1,                 type=int)
    parser.add_option('-l', '--lumi',         dest='lumi' ,        help='lumi to print out',              default=12900,              type=float)
    parser.add_option(      '--lumiSpecs',    dest='lumiSpecs',    help='lumi specifications for some channels [tag:lumi,tag2:lumi2,...]', default=None,       type=str)
    parser.add_option(      '--only',         dest='only',         help='plot only these (csv)',          default='',                type='string')
    parser.add_option(      '--skip',         dest='skip',         help='skip these samples (csv)',       default='MC13TeV_TTJets_cflip',                type='string')
    parser.add_option(      '--fdir',         dest='fdir',         help='directory in file [%default]',   default=None,                type='string')
    parser.add_option(      '--pSysts',       dest='pSysts',       help='proc to plot systs [%default]',  default='t#bar{t}',          type='string')
    parser.add_option(      '--puNormSF',     dest='puNormSF',     help='Use this histogram to correct pu weight normalization', default=None, type='string')
    parser.add_option(      '--procSF',       dest='procSF',       help='Use this to scale a given process component e.g. "W":.wjetscalefactors.pck,"DY":dyscalefactors.pck', default=None, type='string')
    (opt, args) = parser.parse_args()

    #read lists of samples
    samplesList=[]
    jsonList = opt.json.split(',')
    for jsonPath in jsonList:
        jsonFile = open(jsonPath,'r')
        samplesList += json.load(jsonFile, encoding='utf-8', object_pairs_hook=OrderedDict).items()
        jsonFile.close()
    
    #read lists of syst samples
    systSamplesList=[]
    if opt.systJson:
        systJsonList = opt.systJson.split(',')
        for jsonPath in systJsonList:
            jsonFile = open(jsonPath,'r')
            systSamplesList += json.load(jsonFile,encoding='utf-8').items()
            jsonFile.close()

    #read list of signal samples
    signalSamplesList=None
    try:
        jsonFile = open(opt.signalJson,'r')
        signalSamplesList=json.load(jsonFile, encoding='utf-8', object_pairs_hook=OrderedDict).items()
        jsonFile.close()
    except:
        pass
    
    skipList=opt.skip.split(',')

    #lumi specifications per tag
    lumiSpecs={}
    if opt.lumiSpecs:
        for spec in opt.lumiSpecs.split(','):
            tag,lumi=spec.split(':')
            lumiSpecs[tag]=float(lumi)

    #proc SF
    procSF={}
    if opt.procSF:
        procList=opt.procSF.split(',')
        for newProc in procList:
            proc,cacheUrl=newProc.split(':')
            if not os.path.isfile(cacheUrl) : continue
            cache=open(cacheUrl,'r')
            procSF[proc]=pickle.load(cache)
            cache.close()
            print 'Scale factors added for',proc

    onlyList=opt.only.split(',')

    #read plots 
    plots=OrderedDict()

    report=''
    for slist,isSignal,isSyst in [ (samplesList,False,False),(signalSamplesList,True,False),(systSamplesList,False,True) ]:
        if slist is None: continue
        for tag,sample in slist: 
            if isSyst and not 't#bar{t}' in sample[3] : continue
            if tag in skipList:
              print("SKIPPED "+tag)
              continue
            xsec=sample[0]
            isData=sample[1] 
            isDataDriven=sample[2]
            title=sample[3]
            color=sample[4]
            
            fIn=ROOT.TFile.Open('%s/%s.root' % ( opt.inDir, tag) )
            if not fIn : continue
            
            keyList=fIn.Get(opt.fdir).GetListOfKeys() if opt.fdir else fIn.GetListOfKeys()
            for tkey in keyList:
                keyIsSyst=False

                try:
                    key=tkey.GetName()

                    #filter plots using a selection list
                    keep=False if len(onlyList)>0 else True
                    for pname in onlyList: 
                        if pname in key: 
                            keep=True
                            break
                    if not keep: continue
                        
                    #build collection of histos (in case it's a syst type of histogram)
                    histos = []
                    obj=tkey.ReadObj()
                    if obj.InheritsFrom('TH2'):
                        if key[-5:]=='_syst':
                            if title!=opt.pSyst: continue
                            keyIsSyst=True
                            key = key[:-5]
                            for ybin in xrange(1,obj.GetNbinsY()+1):
                                for xbin in xrange(0,obj.GetNbinsX()+2):
                                    if math.isnan(obj.GetBinContent(xbin, ybin)):
                                        obj.SetBinContent(xbin, ybin, 0)
                                        obj.SetBinError(xbin, ybin, 0)
                                weighthist = obj.ProjectionX('_px'+str(ybin), ybin, ybin)
                                weighthist.Draw() #do we need to draw it?
                                if (weighthist.Integral() > 0): histos.append(weighthist)
                        else:
                            histos.append(obj)

                    else:
                        histos.append(obj)
                        histos[-1].SetTitle(title)
                    
                    #finalize histograms
                    for hist in histos:
                        
                        #fix extremities
                        fixExtremities(hist, False, False)
                        histos[-1].SetTitle(title)
                        
                        #rebin if needed
                        if opt.rebin>1:  hist.Rebin(opt.rebin)

                        #scale by lumi or special scape factor
                        if not isData and not isDataDriven:
                            
                            sfVal=1.0                            
                            for procToScale in procSF:
                                if title==procToScale:
                                    for pcat in procSF[procToScale]:                                    
                                        if pcat not in key: continue
                                        sfVal=procSF[procToScale][pcat][0]
                                        break

                            lumi=opt.lumi
                            for tag in lumiSpecs:
                                if not tag in key: continue
                                lumi=lumiSpecs[tag]
                                break
                            
                            hist.Scale(xsec*lumi*sfVal)
                            
                        #create new plot if needed
                        if not key in plots : 
                            plots[key]=Plot(key,com=opt.com)
                            plots[key].doMCOverData=False if opt.doDataOverMC else True

                        #add process to plot
                        plots[key].add(h=hist,title=hist.GetTitle(),color=color,isData=isData,spImpose=isSignal,isSyst=(isSyst or keyIsSyst))
                            
                except Exception,e:
                    print '<'*50
                    print e
                    print '<'*50
                    pass

    #show plots
    ROOT.gStyle.SetOptTitle(0)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gROOT.SetBatch(True)
    if (not opt.outDir): outDir = opt.inDir+'/plots'
    else:                outDir = opt.outDir
    os.system('mkdir -p %s' % outDir)
    os.system('rm %s'%os.path.join(outDir,opt.outName))
    for p in plots : 
        plots[p].mcUnc=opt.mcUnc
        if opt.saveLog    : plots[p].savelog=True
        skipPlot=False
        if opt.onlyData and plots[p].dataH is None: skipPlot=True 
        if opt.silent : skipPlot=True
        if not skipPlot : 
            plots[p].cmsLabel=opt.cmsLabel
            plots[p].plotformats=opt.formats.split(',')
            if opt.noUncs: plots[p].drawUnc=False
            if opt.scaleToData: plots[p].scaleToData()
            plots[p].show(outDir=outDir,lumi=opt.lumi,noStack=opt.noStack,saveTeX=opt.saveTeX,noRatio=opt.noRatio)
            
        plots[p].appendTo('%s/%s'%(outDir,opt.outName))
        plots[p].reset()

    print '-'*50
    print 'Plots and summary ROOT file can be found in %s' % outDir
    if len(report) : print report
    print '-'*50

        
"""
for execution from another script
"""
if __name__ == "__main__":
    sys.exit(main())

