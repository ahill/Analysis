#! /usr/bin/env python
import os, sys
import ROOT
import optparse
import re

def isint(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

def tryint(s):
    return int(s) if isint(s) else s

def alphanum_key(s):
    return [ tryint(c) for c in re.findall('^.*([0-9]+).*',s) ] #re.split('([0-9]+)', s) ]


def getBaseNames(dirname,separator='.chunk'):
    """determine base names"""

    counters = {}
    badFiles = []


    names = set()
    for item in os.listdir(dirname):
        filename, ext = os.path.splitext(item)
        if not ext == '.root': continue
        try:

            #check if file is ok
            fIn=ROOT.TFile.Open(dirname+'/'+item)
            goodFile = False
            try:
                if fIn and not fIn.IsZombie() and not fIn.TestBit(ROOT.TFile.kRecovered):
                    goodFile = True
            except:
                pass
            if (not goodFile):
                badFiles.append(dirname+'/'+item)
                continue

            #get file number
            basename, number = filename.rsplit(separator,1)
            if not number == 'missing' and not isint(number):
                raise ValueError
            try:
                counters[basename].append(dirname+'/'+item)
            except KeyError:
                counters[basename] = [dirname+'/'+item]
            names.add(basename)
        except ValueError:
            print filename,'is single'
            names.add(filename)
            counters[filename]=[dirname+'/'+item]

    #sort numerically
    for key in counters:
        counters[key].sort(key=alphanum_key)

    return names,counters,badFiles


def main():
    parser=optparse.OptionParser(usage='%prog [options]')
    parser.add_option("--sep",  dest="separator", type='string',  default=".chunk", help='count postfix [%default]')
    parser.add_option("--hadd", dest="haddOpts", type='string',  default='-ff',       help='hadd options [%default]')
    (opt, args) = parser.parse_args()

    inputdir = args[0]
    outputdir=inputdir
    if len(args)>1: outputdir=args[1]
    if not os.path.isdir(inputdir):
        print "Input directory not found:", inputdir
        exit(-1)

    if outputdir==inputdir:
        os.system('mkdir -p %s/Chunks' % outputdir)

    basenames,counters,badFiles = getBaseNames(inputdir,opt.separator)
    print '-----------------------'
    print 'Will process the following samples:', basenames

    for basename, files in counters.iteritems():

        if outputdir==inputdir and len(files)==1:
            print 'Skipping single file',basename
            continue

        filenames = " ".join(files)
        target = "/tmp/%s.root" % basename

        # merging:
        print '... processing', basename
        cmd = 'hadd %s %s %s' % (opt.haddOpts, target, filenames)        
        print cmd
        os.system(cmd)

        #mv to output dir
        os.system('mv -v %s %s/'%(target,outputdir))
        
        if outputdir==inputdir and len(files)>1:
            for f in files:
                os.system('mv %s %s/Chunks'%(f,inputdir))

    if (len(badFiles) > 0):
        print '<'*50
        print 'The following files are not done yet or require resubmission, please check job outputs:'
        for file in badFiles: 
            print file,
        print '<'*50

if __name__ == "__main__":
    main()
