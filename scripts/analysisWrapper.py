import sys
import os
import optparse
from importlib import import_module
import ROOT

def runAnalysisPacked(task):
    """Abstracts the analysis modules to be called so that multithread can be used"""
    try:
        
        #build up the TChain and make friends if necessary
        tchains=[]
        mods,urls,out,args=task         
        for x,y in urls:
            tchains.append( ROOT.TChain(y) )
            tchains[-1].AddFile(x)
            if len(tchains)==1: continue
            tchains[0].AddFriend( tchains[-1] )

        #tag of the file
        args += ' --tag %s'%( os.path.basename(out).split('_part')[0] )

        print out

        #run the modules
        outF=ROOT.TFile.Open(out+'.root','RECREATE')
        for mod in mods:
            ana = mod(args)
            ana._beginJob(outF)
            ana._run(tree=tchains[0])
            ana._finalize(outF)
        outF.Close()
        print 'Output available in %s.root',out

    except  Exception, e:
        print 50*'<'
        print '[runAnalysisPacked][Warning] problem with',task,'continuing without. Exception follows'
        print e
        print 50*'<'
        return False
    return True

def main():

    #readout the configuration
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage)
    parser.add_option('-o', '--out',    
                      dest='output', 
                      help='output name',
                      default='results/',
                      type='string')
    parser.add_option('-i', '--in',    
                      dest='inputs', 
                      help='input dirs or files',                       
                      default='/eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest/,/eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest_post/',
                      #default='/eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest/WWToLNuQQ/treeProducerWMass/tree.root,/eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest_post/tree_Friend_WWToLNuQQ.root',   
                      type='string')
    parser.add_option('--args',    
                      dest='args', 
                      help='analysis arguments [%default]',
                      default=None,
                      type='string')
    parser.add_option('-f', '--files', 
                      dest='files',  
                      help='file names [%default]',                      
                      default='{0}/treeProducerWMass/tree.root,tree_Friend_{0}.root',
                      type='string')
    parser.add_option('-t', '--trees', 
                      dest='trees',  
                      help='tree names [%default]',                      
                      default='tree,Friends',
                      type='string')
    parser.add_option('--only',
                      dest='onlyList',  
                      help='process only this [%default]',                      
                      default=None,
                      type='string')
    parser.add_option('-j', '--jobs',  
                      dest='jobs',   
                      help='jobs to run in parallel [%default]',     
                      default=1,   
                      type=int)
    parser.add_option('-m', '--modules',    
                      dest='modules', 
                      help='modules to run [%default]',
                      default='UserCode.DataAnalysis.wmass.recoilAnalysis',
                      type='string')
    parser.add_option('-q', '--queue', 
                      dest='queue',  
                      help='Batch queue to use [default: %default]', 
                      default='local')
    parser.add_option('--farm',
                      dest='FARM',
                      help='scripts FARM for condor [%default]',
                      default='FARM',
                      type='string')
    (opt, args) = parser.parse_args()

    #import the required modules
    mods=[]
    for mod in opt.modules.split(','):
        import_module(mod)
        mods.append( getattr(sys.modules[mod],'Analysis') )

    #prepare the tasks
    task_list=[]
    inputs=opt.inputs.split(',')
    trees=opt.trees.split(',')
    onlyList=None if not opt.onlyList else opt.onlyList.split(',')

    if os.path.isfile(inputs[0]):         

        #prepare the output
        os.system('mkdir -p %s'%os.path.dirname(opt.output))

        #single file task list
        task_list=[ (mods,
                     [(inputs[i],trees[i]) for i in xrange(0,len(inputs))],
                     opt.output,
                     opt.args) 
                    ]
    else:

        #prepare the output
        os.system('mkdir -p %s'%opt.output)

        #loop over files in directory and try to match according to file names given to build the task list
        files=opt.files.split(',')
        for p in os.listdir(inputs[0]):

            #filter if required
            if onlyList:
                doIt=False
                for tag in onlyList:
                    if not tag in p : continue
                    doIt=True
                    break
                if not doIt: 
                    continue
            
            #check urls
            urls=[]
            for i in xrange(0,len(inputs)):
                url=os.path.join(inputs[i],files[i].format(p))
                if not os.path.isfile(url) : continue
                urls.append(url)

            if len(urls)!= len(inputs) or len(urls)==0 :
                print 'Skipping',p
                continue
                
            task_list.append( (mods,
                               [(urls[i],trees[i]) for i in xrange(0,len(urls))],
                               os.path.join(opt.output,p),
                               opt.args) )

    #run jobs
    if opt.queue=='local':
        if opt.jobs>1:
            print ' Running %d jobs in %d threads' % (len(task_list), opt.jobs)
            import multiprocessing as MP
            pool = MP.Pool(opt.jobs)
            pool.map(runAnalysisPacked,task_list)
        else:
            for t in task_list:
                runAnalysisPacked(t)
    else:

        os.system('mkdir -p FARM')

        #dump a bash wrapper to set the environment and call this script with the arguments to be passed
        with open('{0}/analysisWrapper.sh'.format(opt.FARM),'w') as wrapper:
            cmssw=os.environ['CMSSW_BASE']
            wrapper.write('#!/bin/bash\n')
            wrapper.write('\n#keep quotes in the argument\n')
            wrapper.write('whitespace="[[:space:]]"\n')
            wrapper.write('ARGS=""\n')
            wrapper.write('for i in "$@"\n')
            wrapper.write('do\n')
            wrapper.write('    if [[ $i =~ $whitespace ]]\n')
            wrapper.write('    then\n')
            wrapper.write('        i=\"$i\"\n')
            wrapper.write('    fi\n')
            wrapper.write('    ARGS="${ARGS} $i"\n')
            wrapper.write('done\n')
            wrapper.write('\n#setup environment\n')
            wrapper.write('WORKDIR=`pwd`\n')
            wrapper.write('CMSSW=%s\n'%cmssw)
            wrapper.write('cd $CMSSW/src\n')
            wrapper.write('eval `scram r -sh`\n')
            wrapper.write('cd $WORKDIR\n')
            wrapper.write('\n#run the python analysisWrapper.py\n')
            wrapper.write('echo "Running analysisWrapper.py from $WORKDIR @ `hostname`"\n')
            wrapper.write('echo "Passing the following arguments: ${ARGS}"\n')
            wrapper.write('eval "python %s/src/UserCode/DataAnalysis/scripts/analysisWrapper.py ${ARGS}"'%cmssw)
            
        #fill a condor script with the arguments to pass to the bash wrapper
        os.system('mkdir -p {0}/logs'.format(opt.FARM))
        with open ('%s/condor.sub'%opt.FARM,'w') as condor:
            condor.write('executable = {0}/analysisWrapper.sh\n'.format(opt.FARM))
            condor.write('output     = {0}/logs/output_$(jobName).out\n'.format(opt.FARM))
            condor.write('error      = {0}/logs/output_$(jobName).err\n'.format(opt.FARM))
            condor.write('log        = {0}/logs/output_$(jobName).log\n'.format(opt.FARM))
            condor.write('+JobFlavour = \"%s\"\n'%opt.queue)
            for task in task_list:
                _,urls,out,args=task                         
                files = ','.join([ u for u,_ in urls ])
                condor.write('jobName =%s\n'%os.path.basename(out))
                condor.write('arguments =-q local -m {0} -t {1} -o {2} -i {3} -f {4} --args \\\"{5}\\\"\n'.format(opt.modules,opt.trees,out,files,opt.files,opt.args))
                condor.write('queue 1\n')

        #send to condor
        os.system('condor_submit %s/condor.sub'%opt.FARM)


if __name__ == "__main__":
    sys.exit(main())
