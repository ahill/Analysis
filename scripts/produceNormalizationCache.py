#!/usr/bin/env python

import optparse
import os,sys
import ROOT
import pickle
import re

def main():
    """ loops over a directory and collects weights used for normalization, pileup weighting etc. """

    #configuration
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage)
    parser.add_option('-i', '--inDir',       
                      dest='inDir',
                      help='input directory with files [%default]',   
                      default='/eos/cms/store/cmst3/user/psilva/Wmass/RecoilTest', 
                      type='string')
    parser.add_option('--histoList',   
                      dest='histoList',   
                      help='histoList file1:histo1,file2:histo2 [%default]',  
                      default='treeProducerWMass/tree.root:SumGenWeights+tree.nTrueInt(100,0,100)',
                      type='string')
    parser.add_option('-o', '--output',
                      dest='cache',       
                      help='output file [%default]',                  
                      default='data/wmass/GenInfo.pck',
                      type='string')
    (opt, args) = parser.parse_args()

    GenInfo={}
    histoList=[ tkn.split(':') for tkn in opt.histoList.split(';') ]    
    for sample in os.listdir(opt.inDir):

        tag=sample.split('_part')[0]
        if not tag in GenInfo: GenInfo[tag]={}

        for url,histos in histoList:
            fIn=ROOT.TFile.Open(os.path.join(opt.inDir,sample,url))

            for histo in histos.split('+'):
                if not '.' in histo:
                    h=fIn.Get(histo)
                else:                  
                    histoDef=re.findall('[-+]?[0-9]*\.?[0-9]+',histo)
                    treeName,histo=re.findall('(\w+)\.(\w+)',histo)[0]
                    tree=fIn.Get(treeName)
                    tree.Draw('%s>>h(%s,%s,%s)'%(histo,histoDef[0],histoDef[1],histoDef[2]),'','goff')
                    h=ROOT.gDirectory.Get('h')

                try:
                    if not histo in GenInfo[tag]:
                        GenInfo[tag][histo]=h.Clone(histo)
                        GenInfo[tag][histo].SetTitle(tag)
                        GenInfo[tag][histo].Reset('ICE')
                        GenInfo[tag][histo].SetDirectory(0)
                    GenInfo[tag][histo].Add(h)
                except Exception, e:
                    print 50*'<'
                    print 'Failed to add correctly',histo,'for',sample
                    print e
                    print 50*'<'

            fIn.Close()
        
    os.system('mkdir -p {0}'.format( os.path.dirname(opt.cache) ) )
    with open(opt.cache,'w') as cachefile:
        pickle.dump(GenInfo, cachefile, pickle.HIGHEST_PROTOCOL)
    print 'Summed histograms for',GenInfo.keys(),'have been stored in',opt.cache

    exit(0)


if __name__ == "__main__":
    sys.exit(main())
