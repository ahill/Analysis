# DataAnalysis codes

## Installation

To clone the repository locally do
```
git clone ssh://git@gitlab.cern.ch:7999/psilva/Analysis.git  UserCode/DataAnalysis
```

## Structure

The following main directories used are

* scripts - generic scripts common to all analysis
* python  - analysis code
* test    - standalone scripts
* cfg     - configuration files (e.g. cmssw, heppy)
* data    - analysis specific data (e.g. normalization, pileup, scale factors cache files)
* doc     - analysis specific documentation in markdown style

Use a common designation for an analysis (e.g. wmass) and use document it
as wmass.md in doc and add appropriate sub-directories to python,test,cfg,data, etc.

## Running one or several analysis

A wrapper to call analysis modules is available in scripts/analysisWrapper.py.
It can run locally or submit jobs to condor.
A base class python/AnalysisBase.py can be used to derive specific analyses classes from it.

## Merging and plotting 

A merging script can be used to merge chunks produced by an analysis in scripts/mergeOutputs.py
A plotting script is used to run over the outputs of the analysis in scripts/plotter.py